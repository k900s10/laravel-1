<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return view('page/welcome');
});
*/
/*
use App\Http\Controllers\testingController;
Route::get('/home', [testingController::class, 'index']);
*/

use App\Http\Controllers\HomeController;
Route::get('/', [HomeController::class, 'welcome']);

Route::get('register', [HomeController::class, 'register']);

use App\Http\Controllers\AuthController;
Route::post('redirect', [AuthController::class, 'redirect']);