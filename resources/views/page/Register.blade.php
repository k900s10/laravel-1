<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <br>
    <h3>Sign Up Form</h3>
    <form class="" action="{{url('redirect')}}" method="post">
        @csrf
        <p>First Name: </p>
        <input type="text" name="fname" value="" >
        <p>Last Name: </p>
        <input type="text" name="lname" value="">
        <p>Gender: </p>
        <input type="radio" name="gender1" value="male">male
        <br>
        <input type="radio" name="gender2" value="female">female
        <br>
        <input type="radio" name="gender3" value="other">other

        <br>
        <p>Nationality: </p>
        <select class="" name="Nationality">
          <option name="Nationality1" value="Indonesian">Indonesian</option>
          <option name="Nationality12" value="Other">Other</option>
        </select>

        <br>
        <p>Language Spoken: </p>
        <input type="checkbox" name="Language[]" value="Bahasa Indonesia">Bahasa Indonesia
        <br>
        <input type="checkbox" name="Language[]" value="English">English
        <br>
        <input type="checkbox" name="Language[]" value="Other">Other

        <br>
        <p>Bio: </p>
        <textarea name="Bio" rows="8" cols="40"></textarea>

        <br>
        <input type="submit" name="" value="Sign up">
      </form>
  </body>
</html>
